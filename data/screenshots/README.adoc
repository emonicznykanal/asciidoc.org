= Screenshot settings

== Requirements

- HiDPI screen (4K)
- DPI: 148
- Shutter (or smiliar) to take screenshots without the frame
- Firefox with dark color scheme
- Edit Firefox toolbar to be compact and remove flexible spaces
- Screenshot size: 2048x1152 (16/9)
  * Hero screenshot size: 3840x2400 (16/10); then cropped to 2048x940
- Screenshots should not include border radius or drop-shadow
- Node 16
- `npm i`

== Generate screenshots with frame

. Put images in the `img` directory
. Run `node index.js`

Screenshots are generated in the `dist` directory
