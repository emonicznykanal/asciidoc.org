import { ImagePool } from '@squoosh/lib'
import { cpus } from 'os'
import fs from 'fs/promises'
import { dirname, join, parse } from 'path'
import { fileURLToPath } from 'url'

const __dirname = dirname(fileURLToPath(import.meta.url))

const imagePool = new ImagePool(cpus().length)

const imagesDirectory  = join(__dirname, 'img')
const distDirectory  = join(__dirname, 'dist')

const imageNames = await fs.readdir(imagesDirectory)
for (let imageName of imageNames) {
  if (imageName.endsWith('.png') || imageName.endsWith('.jpeg') || imageName.endsWith('.jpg')) {
    console.log(`Processing ${imageName}...`)
    const file = await fs.readFile(join(imagesDirectory, imageName))
    const image = imagePool.ingestImage(file)
    await image.preprocess({
      quant: {
        numColors: 256,
        dither: 1
      }
    })
    await image.encode({ oxipng: { level: 3 }, webp: { quality: 85 } })
    const oxipngEncodedImage = await image.encodedWith.oxipng
    await fs.writeFile(join(distDirectory, imageName), oxipngEncodedImage.binary)
    const webpEncodedImage = await image.encodedWith.webp
    await fs.writeFile(join(distDirectory, `${parse(imageName).name}.webp`), webpEncodedImage.binary)
  }
}

await imagePool.close();
