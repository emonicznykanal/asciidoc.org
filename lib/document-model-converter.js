const asciidoctor = require('@asciidoctor/core')()
const { parse, dirname, join } = require('path')

function encodeAttributeValue(val) {
 return val.includes('"') ? val.replaceAll('"', '&quot;') : val
}

class DocumentModelConverter {
  constructor () {
    this.baseConverter = asciidoctor.Html5Converter.create()
    this.result = {}
    this.tabIndex = 1
  }

  convert (node, transform) {
    if (transform === 'embedded') {
      const blocks = node.getBlocks()
      this.result.title = node.getTitle()
      this.result.tagline = node.getAttribute('tagline')
      this.result.description = node.getAttribute('description')
      for (const block of blocks) {
        if (block.getNodeName() === 'preamble') {
          this.result['preamble'] = {
            contents: block.getContent()
          }
        } else if (block.getNodeName() === 'section') {
          const id = block.getId()
          const contents = []
          const sectionBlocks = block.getBlocks()
          const title = block.getTitle()
          const titleSeparator = block.getAttribute('separator', ':')
          const [main, subtitle] = title.split(titleSeparator)
          const cards = []
          const tabs = []
          for (const sectionBlock of sectionBlocks) {
           if (sectionBlock.getContext() === 'open' && sectionBlock.getRoles().includes('tab')) {
             let tabTitle
             let tabId
            const tabContentBlocks = []
             if (sectionBlock.getRoles().includes('compare')) {
               const floatingTitleBlock = sectionBlock.findBy({context: 'floating_title'})
               if (floatingTitleBlock && floatingTitleBlock.length) {
                 tabTitle = floatingTitleBlock[0].getTitle()
                 tabId = floatingTitleBlock[0].getId()
               }
               const sourceBlocks = sectionBlock.findBy({context : 'listing'})
               const explanationBlocks = sectionBlock.findBy({context : 'paragraph'})
               const tabContents = {}
               for (const i in sourceBlocks) {
                 const sourceBlock = sourceBlocks[i]
                 const lang = sourceBlock.getAttribute('language')
                 const explanation = explanationBlocks.length > i ? explanationBlocks[i].convert() :  ''
                 const tabContentKey = lang === 'asciidoc' ? 'to' : 'from'
                 const sourceBlockTitle = sourceBlock.getTitle()
                 sourceBlock.setTitle(undefined)
                 if (tabContentKey in tabContents) {
                   tabContents[tabContentKey].files.push({
                     name: sourceBlockTitle,
                     contents: sourceBlock.convert()
                   })
                 } else {
                   tabContents[tabContentKey] = {
                     lang,
                     index: this.tabIndex++,
                     files: [{
                       name: sourceBlockTitle,
                       contents: sourceBlock.convert()
                     }],
                     explanation
                   }
                 }
               }
               tabs.push({
                 title: tabTitle,
                 id: tabId,
                 contents: tabContents
               })
             } else {
               for (const tabBlock of sectionBlock.getBlocks()) {
                 if (tabBlock.getContext() === 'floating_title') {
                   tabTitle = tabBlock.getTitle()
                   tabId = tabBlock.getId()
                 } else {
                   tabContentBlocks.push(tabBlock)
                 }
               }
               tabs.push({
                 title: tabTitle,
                 id: tabId,
                 contents: tabContentBlocks.map((b) => b.convert()).join('\n')
               })
             }
            } else if (sectionBlock.getId() === 'editor-code') {
              contents.code = sectionBlock.getSource()
            } else if (sectionBlock.getContext() === 'open' && sectionBlock.getRoles().includes('card')) {
              cards.push({
                contents: sectionBlock.convert()
              })
            } else {
              contents.push({
                type: 'html',
                contents: sectionBlock.convert()
              })
            }
          }
          if (tabs.length) {
            contents.push({
              type: 'tabs',
              index: this.tabIndex++,
              items: tabs
            })
          }
          if (cards.length) {
            contents.push({
              type: 'cards',
              items: cards
            })
          }
          this.result[id] = {
            title: main,
            subtitle: subtitle?.trim(),
            contents,
          }
        }
      }
      return this.result
    }
    const nodeName = node.getNodeName()
    if (nodeName === 'image') {
      const target = node.getAttribute('target')
      if (target.endsWith('.svg')) {
        return this.baseConverter.convert(node, transform)
      }
      const img = `<img src="${node.getImageUri(target)}" alt="${encodeAttributeValue(node.getAlt())}"/>`
      const mimeType = target.endsWith('png') ? 'image/png' : 'image/jpeg'
      const targetInfo = parse(target)
      const role = node.getRole()
      return `<div class="imageblock${role ? ' ' + role : ''}">
  <picture class="content">
    <source srcset="${targetInfo.dir}/${targetInfo.name}.webp" type="image/webp">
    <source srcset="${node.getImageUri(target)}" type="${mimeType}">
    ${img}
  </picture>
</div>`
    }
    return this.baseConverter.convert(node, transform)
  }
}

module.exports = DocumentModelConverter
