export function init () {
  const hash = window.location.hash
  find('.tabset').forEach(function (tabset) {
    let active
    const tabs = tabset.querySelector('.tabs')
    if (tabs) {
      let first
      find('li', tabs).forEach(function (tab, idx) {
        const id = (tab.querySelector('a[id]') || tab).id
        if (!id) return
        const pane = getPane(id, tabset)
        if (!idx) first = { tab, pane }
        if (!active && hash === '#' + id && (active = true)) {
          tab.classList.add('is-active')
          if (pane) pane.classList.add('is-active')
        } else if (!idx) {
          tab.classList.remove('is-active')
          if (pane) pane.classList.remove('is-active')
        }
        tab.addEventListener('click', activateTab.bind({ tabset, tab, pane, id }))
      })
      if (!active && first) {
        first.tab.classList.add('is-active')
        if (first.pane) first.pane.classList.add('is-active')
      }
    }
    const select = tabset.querySelector('.select > select')
    if (select) {
      select.addEventListener('change', (event) => {
        console.log(event)
        const id = event.target.value
        if (!id) return
        const anchor = document.getElementById(id)
        const tab = anchor.parentElement.parentElement
        const pane = getPane(id, tabset)
        activateTab.bind({ tabset, tab, pane, id })(event)
      })
    }
    tabset.classList.remove('is-loading')
  })

  function activateTab (e) {
    const tab = this.tab
    const pane = this.pane
    const id = this.id
    const tabset = this.tabset
    if (id && tabset) {
      const select = tabset.querySelector('.select > select')
      if (select) {
        select.value = id
      }
    }
    find(':scope > .tabs li', this.tabset).forEach(function (it) {
      it === tab ? it.classList.add('is-active') : it.classList.remove('is-active')
    })
    find(':scope > .content > .tab-pane', this.tabset).forEach(function (it) {
      it === pane ? it.classList.add('is-active') : it.classList.remove('is-active')
    })
    e.preventDefault()
  }

  function find (selector, from) {
    return Array.prototype.slice.call((from || document).querySelectorAll(selector))
  }

  function getPane (id, tabset) {
    return find('.tab-pane', tabset).find(function (it) {
      return it.getAttribute('aria-labelledby') === id
    })
  }
}
